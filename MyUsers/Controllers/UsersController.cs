﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyUsers.DbModels;
using MyUsers.Models;
using MyUsers.Services;

namespace MyUsers.Controllers;
[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private MyDbContext _db;
    private readonly IAzureConnector _ac;
    private readonly UserRepo _ur;

    public UsersController(IAzureConnector ac, UserRepo ur)
    {
        _ac = ac;
        _ur = ur;
    }

    public CreateUserResponse CreateUser(CreateUserRequest model)
    {
        if (model == null)
        {
            throw new ApplicationException();
        }

        bool result = false;
        if (model.Birthday.HasValue)
        {
            result = _ac.SendBirthday(model.Name, model.Birthday.Value);
        }

        var u = _ur.SaveUser(model);

        return new CreateUserResponse
        {
            Id = u.Id,
            Name = u.Name,
            Birthday = model.Birthday,
            AzureResult = result
        };
    }
}
