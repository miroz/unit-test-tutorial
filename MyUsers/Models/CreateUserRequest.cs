﻿namespace MyUsers.Models;

public class CreateUserRequest
{
    public string Name { get; set; }
    public DateTime? Birthday { get; set; }
}

public class CreateUserResponse
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime? Birthday { get; set; }
    public bool AzureResult { get; internal set; }
}