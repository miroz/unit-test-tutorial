﻿using MyUsers.DbModels;
using MyUsers.Models;

namespace MyUsers.Services;

public class UserRepo
{
    private readonly MyDbContext _db;

    public UserRepo(MyDbContext db)
    {
        _db = db;
    }
    public User SaveUser(CreateUserRequest model)
    {
        var u = new User
        {
            Name = model.Name
        };
        _db.Add(u);
        _db.SaveChanges();
        return u;
    }
}
