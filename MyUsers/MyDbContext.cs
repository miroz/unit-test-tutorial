﻿using Microsoft.EntityFrameworkCore;
using MyUsers.DbModels;

namespace MyUsers;

public class MyDbContext : DbContext
{
    public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
    {

    }

    public DbSet<User> Users { get; set; }
}
