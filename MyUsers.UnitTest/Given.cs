﻿using MyUsers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyUsers.UnitTest;
internal class Given
{
    internal static CreateUserRequest UserRequest(string name, DateTime? birthday)
    {
        var model = new CreateUserRequest()
        {
            Name = name,
            Birthday = birthday
        };
        return model;
    }
}
