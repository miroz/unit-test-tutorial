using MyUsers.Controllers;
using MyUsers.Models;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using MyUsers;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MyUsers.Services;
using Moq;

namespace MyUsers.UnitTest;

public class UserTest
{
    private readonly MyDbContext _db;
    private readonly Mock<IAzureConnector> _ac;
    private readonly UserRepo _ur;
    private readonly UsersController _uc;

    public UserTest()
    {
        _db = CreateMockDbContext("testname");

        _ac = new Mock<IAzureConnector>();
        _ur = new UserRepo(_db);
        _uc = new UsersController(_ac.Object, _ur);

    }

    [Fact]
    public void CreateUser_ShouldReturn400_IfRequestNull()
    {
        var uc = new UsersController(null, null);
        Assert.Throws<ApplicationException>(() => uc.CreateUser(null));
    }

    [Fact]
    public void CreateUser_Should_AddToDb()
    {

        var model = Given.UserRequest("Miro", null);

        // when
        var response = _uc.CreateUser(model);


        //then        
        response.Should().NotBeNull();
        response.Name.Should().Be(model.Name);
        Assert.Null(response.Birthday);
        Assert.True(response.Id > 0);

        _db.Users.Count().Should().Be(1);
        var dbUser = _db.Users.First();
        dbUser.Name.Should().Be(model.Name);

        Assert.True(response.Id > 0);
        Assert.True(response.Id == dbUser.Id);
    }

    [Fact]
    public void CreateUserWithBirthday_Shoud_SendToQueue()
    {
        var modelBirthday = Given.UserRequest("Miro", DateTime.Now.AddYears(-25));
        var modelNoBirthday = Given.UserRequest("Miro2", null);

        _ac.Setup(m => m.SendBirthday(modelBirthday.Name, modelBirthday.Birthday.Value)).Returns(true);

        // when
        var response = _uc.CreateUser(modelBirthday);
        response.AzureResult.Should().Be(true);
        _ac.VerifyAll();
        //_ac.Verify(m => m.SendBirthday(modelBirthday.Name, modelBirthday.Birthday.Value), Times.Once);

        response = _uc.CreateUser(modelNoBirthday);
        response.AzureResult.Should().Be(false);

    }

    public static MyDbContext CreateMockDbContext(string dbName = "TestDbName")
    {
        var options = new DbContextOptionsBuilder<MyDbContext>()
           // InMemory DB does not support transactions, so disable the transaction warning
           .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
           .UseInMemoryDatabase(databaseName: dbName)
           .EnableSensitiveDataLogging()
           .Options;

        return new MyDbContext(options);
    }
}